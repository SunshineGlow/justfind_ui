import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss']
})
export class FilterListComponent implements OnInit {
  public ads = [
    {
      title: 'Объявление 1',
      price: 125000,
      color: '#ff8400',
      city: 'г. Севастополь',
      creationDate: new Date()
    },
    {
      title: 'Объявление 2',
      price: 125000,
      color: '#ff8400',
      city: 'г. Севастополь',
      creationDate: new Date()
    },
    {
      title: 'Объявление 3',
      price: 125000,
      color: '#ff8400',
      city: 'г. Севастополь',
      creationDate: new Date()
    },
    {
      title: 'Объявление 4',
      price: 125000,
      color: '#ff8400',
      city: 'г. Севастополь',
      creationDate: new Date()
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
